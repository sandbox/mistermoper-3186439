<?php

namespace Drupal\multilingual_plus\Plugin\Webformhandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerMessageInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Plugin\WebformHandler\EmailWebformHandler;

/**
 * Emails a webform submission with different recipient per language.
 *
 * @WebformHandler(
 *   id = "email_language",
 *   label = @Translation("Email per language"),
 *   category = @Translation("Notification"),
 *   description = @Translation("Sends a webform submission via an email. It allows different recipients per language."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class MultilanguageEmailHandler extends EmailWebformHandler implements WebformHandlerMessageInterface {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['multilanguage_recipients'] = [
      '#title' => $this->t('Multilanguage recipients'),
      '#description' => $this->t('Use it to set up different mails per language'),
      '#type' => 'fieldset',
    ];

    $languages = $this->languageManager->getLanguages();
    $default_language = $this->languageManager->getDefaultLanguage();
    foreach ($languages as $language) {
      if ($language->getId() == $default_language->getId()) {
        continue;
      }

      // @todo use a webform yaml input that allows tokens!
      $form['multilanguage_recipients'][$language->getId()] = [
        '#type' => 'email',
        '#title' => $this->t('To (@language)', ['@language' => $language->getName()]),
        '#default_value' => isset($this->configuration['multilanguage_recipients'][$language->getId()]) ? $this->configuration['multilanguage_recipients'][$language->getId()] : NULL,
      ];

    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_configuration = parent::defaultConfiguration();
    $default_configuration['multilanguage_recipients'] = [];
    return $default_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function sendMessage(WebformSubmissionInterface $webform_submission, array $message) {
    $current_language = $this->languageManager->getCurrentLanguage();
    if (!empty($message['multilanguage_recipients'][$current_language->getId()])) {
      $message['to_mail'] = $message['multilanguage_recipients'][$current_language->getId()];
    }
    return parent::sendMessage($webform_submission, $message);
  }

}
