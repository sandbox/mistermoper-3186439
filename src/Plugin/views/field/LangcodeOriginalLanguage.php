<?php

namespace Drupal\multilingual_plus\Plugin\views\field;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Master language field handler.
 *
 * @ViewsField("langcode_original_language")
 */
class LangcodeOriginalLanguage extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Language manager, used to get langcode labels.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setLanguageManager($container->get('language_manager'));
    return $instance;
  }

  /**
   * Set language manager.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language managerr.
   */
  public function setLanguageManager(LanguageManagerInterface $languageManager) {
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    switch ($this->tableAlias) {
      case 'node_field_data':
        $field = 'nid';
        $table = 'node';
        break;

      case 'media_field_data':
        $field = 'mid';
        $table = 'media';
        break;

      default:
        $field = NULL;
        $table = NULL;

    }
    $configuration = [
      'table' => $table,
      'field' => $field,
      'left_table' => $this->tableAlias,
      'left_field' => $field,
    ];

    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $this->query->addRelationship('original_' . $table, $join, $table);
    $this->query->addField($table, 'langcode', 'langcode_original_language');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $values, $field = NULL) {
    if (isset($values->{'langcode_original_language'})) {
      return $values->{'langcode_original_language'};
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $langcode = $this->getValue($values);
    return !empty($langcode) ? $this->languageManager->getLanguage($langcode)->getName() : $langcode;
  }

}
